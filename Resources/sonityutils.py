import math

def m3toSonityOut(m3ClassifiedProfile):
        outJSON={}
        outJSON['age_dist']=m3ClassifiedProfile["age"]
        outJSON['age']=truncate((15*outJSON["age_dist"]["<=18"]+24*outJSON["age_dist"]["19-29"]+34.5*outJSON["age_dist"]["30-39"]+50*outJ$
        outJSON['age_std']=outJSON["age_dist"]["<=18"]*(outJSON['age']-15)**2
        outJSON['age_std']+=outJSON["age_dist"]["19-29"]*(outJSON['age']-24)**2
        outJSON['age_std']+=outJSON["age_dist"]["30-39"]*(outJSON['age']-34.5)**2
        outJSON['age_std']+=outJSON["age_dist"][">=40"]*(outJSON['age']-50)**2
        outJSON['age_std']=truncate(math.sqrt(outJSON["age_std"]),1)            
        outJSON['gender']='male' if m3ClassifiedProfile["gender"]["male"]>m3ClassifiedProfile["gender"]["female"] else 'female'
        outJSON['gender_prob']=m3ClassifiedProfile["gender"][outJSON['gender']]
        outJSON['org']='is-org' if m3ClassifiedProfile["org"]["is-org"]>m3ClassifiedProfile["org"]["non-org"] else 'non-org'
        outJSON['org_prob']=m3ClassifiedProfile["org"][outJSON['org']]


        return outJSON

def truncate(f, n):
        return math.floor(f * 10 ** n) / 10 ** n


