FROM postgres:12.1

RUN apt-get update 
RUN apt-get -y install git python3 postgresql-plpython3-12 python3-pip

RUN apt-get install -y postgresql-12-cron

WORKDIR /usr/bin
RUN ln -s python3 python && ln -s pip3 pip

RUN pip3 install torch==1.3.1+cpu torchvision==0.4.2+cpu -f https://download.pytorch.org/whl/torch_stable.html

WORKDIR /opt
RUN git clone https://github.com/zijwang/m3inference.git

WORKDIR /opt/m3inference

RUN pip3 install -r requirements.txt
RUN python3 setup.py install

RUN pip3 install jsonlines 
RUN pip install Pillow==6.1

RUN  apt-get clean && \
     rm -rf /var/cache/apt/* /var/lib/apt/lists/*



ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 5432
CMD ["-c", "cron.database_name=postgres", "-c", "shared_preload_libraries=pg_cron"]

